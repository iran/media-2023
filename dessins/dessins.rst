.. index::
   ! Dessins

.. _dessins_luttes_iran:

=========================================
Dessins
=========================================

- https://www.macval.fr/Femme-Vie-Liberte

.. https://twitter.com/jul_auteur/status/1576389099564724224?s=20&t=uBmjnbwo1kkGn8dDMPzrCA (le monde est une iranienne)


Todo
=====

- https://www.instagram.com/p/CpdH-P7oO-s/?igshid=MDJmNzVkMjY%3D
- https://www.instagram.com/oudemir/
- https://www.instagram.com/grafikret/
- https://www.instagram.com/berydesigns/
- https://www.instagram.com/samirakhorshidii/
- https://www.instagram.com/m_melgrati/
- https://www.instagram.com/iamahdighazi/
- https://www.instagram.com/shabloolim/

https://www.macval.fr/Femme-Vie-Liberte
===========================================

- https://www.macval.fr/Femme-Vie-Liberte

Sur google drive
===================

- https://drive.google.com/drive/folders/1YABEfj7OKjtZTQaesRWVc90RMsl2GHk6

Mahsa Amini
===============

- https://cartoonmovement.com/collection/mahsa-amini


In Iran, Mahsa Amini (22) fell into a coma and died last week, hours after
the morality police held her for allegedly breaking hijab rules.

According to witnesses, she was beaten while inside a police van that
took her to a detention center. Protests have broken out in over 20 cities in Iran.


Dessins de Bahareh Aakrami
==============================

- :ref:`akrami_iran:dessins_bahareh_akrami`


.. _cut_it:

Cut it
=========

**Cut it** de Marco Melgrati
-------------------------------

- https://twitter.com/Melgratillustr/status/1575427238849581056?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ

Cut It Out! #iran #young  #victim #mahsaamini #amini #woman #protest
#cuthair #blackhair #police #policebrutality #iranwomen #moralitypolice
#illustration #repression #tehran #freedom #hijiab #violence #kill
#murder

@artistic_brs_ @mangobizarre @thepinklemonade @culturainquieta


.. figure:: images/cut_it_1.png
   :align: center
   :width: 500

.. figure:: images/cut_it_2.png
   :align: center
   :width: 500


.. figure:: images/cut_it_4.png
   :align: center
   :width: 500

   Cut it de Marco Melgrati https://twitter.com/Melgratillustr/status/1575427238849581056?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ

**Cut it, Femme, Vie, Liberté**
-----------------------------------

- :ref:`cut_it_grenoble_2022_10_01`

.. figure:: images/cut_it_femme_vie_liberte.png
   :align: center
   :width: 600


Femme, vie, liberté avec des oiseaux
========================================

.. figure:: images/femme_vie_liberte_oiseau.png
   :align: center


Voiles emportées par des oiseaux
==================================


.. figure:: images/voiles_emportées_par_les_oiseaux.png
   :align: center


Niloofar Hamedi, the courageous reporter who broke the story of Mahsa Amini's killing
======================================================================================

- :ref:`niloofar_hamedi`

.. figure:: images/niloofar_hamedi_2.png
   :align: center
   :width: 500


Femme au visage peint (Paris, samedi 1er octobre 2022)
========================================================

.. figure:: images/femme_peinte_paris.png
   :align: center

   https://twitter.com/FaridVahiid/status/1577004532936343553?s=20&t=Z7o1eRxeRgh8WmMOGLF9nw


Le monde est une Iranienne
====================================

#Mahsa_Amini #opIran #WomenLifeFreedom  @Zar_Amir @Golshifteh @amanpour @Paris

.. figure:: images/femme_peinte_paris.png
   :align: center

   https://twitter.com/jul_auteur/status/1576389099564724224?s=20&t=LYhYGl-dX981pZYO5YkJzA



ONU accueil sanglant par Mana Nistani
============================================

.. figure:: onu_accueil_sanglant.png
   :align: center

Femme Vie Liberte, cheveux attachés
=========================================

- :ref:`hadis_najafi`
- :ref:`iran_grenoble_2022_10_01`

.. figure:: images/femme_vie_liberte_cheveux_attaches.png
   :align: center
   :width: 500

.. figure:: images/petite_fille_agitant_un_voile.png
   :align: center
   :width: 500

Virer le roi
=============

.. figure:: images/drapeau_virer_roi.png
   :align: center
   :width: 500


Quelle horreur
===============

.. figure:: images/quelle_horreur_pendus.png
   :align: center
   :width: 500

   Un voyou dit "Quelle horreur !" alors que lui-même assassine les gens

Mollah avec des cordes de pendus sur la tête
==================================================

.. figure:: images/mollah_avec_des_cordes_de_pendus_sur_la_tete.png
   :align: center
   :width: 500


Khomeini bourreau du peuple iranien
==================================================

.. figure:: images/khomeneini_guillotine.png
   :align: center
   :width: 500


Mollah recevant un caca de colombe
==================================================

.. figure:: images/mollah_merde_colombe.png
   :align: center
   :width: 500

Contre la peine de mort
=============================

.. figure:: images/femme_azadi_2023_01_15.png
   :align: center
   :width: 500

Mollah fait de cordes pour pendre et de prison en guise de turban
======================================================================

.. figure:: images/mollah_cordes_prison.png
   :align: center
   :width: 500

