
.. index::
   pair: Grenoble; Samedi 11 mars 2023

.. _images_iran_grenoble_2023_03_11:

===================================================================================================================================================================================================================================================================================================================
**Photos du 25e rassemblement samedi 11 mars 2023 à Grenoble**
===================================================================================================================================================================================================================================================================================================================

- :ref:`iran_luttes:iran_grenoble_2023_03_11`

.. figure:: images/kave.png
   :align: center



.. figure:: images/mise_en_place_affiches.png
   :align: center

.. figure:: images/mise_en_place_affiches_2.png
   :align: center


.. figure:: images/affiches_par_terre.png
   :align: center


.. figure:: images/affiches_par_terre_2.png
   :align: center


.. figure:: images/manif_des_retraites.png
   :align: center

.. figure:: images/manif_des_retraites_2.png
   :align: center


.. figure:: images/marjane_affiches.png
   :align: center



