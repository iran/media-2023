
==========================
2023-09-16
==========================

- :ref:`iran_luttes:iran_grenoble_2023_09_16`


.. _photos_2023_09_16:

Photos de la manifestation du 16 septembre 2023 à Grenoble
===============================================================

- :ref:`iran_luttes:compte_rendus_2023_09_16`

.. figure:: images/tuee_pour_une_meche.png
   :align: center

   Un manifestant brandit un portrait de Mahsa Amini, l'étudiante de 22 ans
   dont le meurtre par la police des mœurs pour un voile "mal porté",
   le 16 septembre 2022, a déclenché le soulèvement en Iran. © Manuel Pavard - Place Gre'net

.. figure:: images/affiches_par_terre.png
   :align: center

.. figure:: images/emmanuel_carroz.png
   :align: center


.. figure:: images/place_felix_poulat_vue_des_militants.png
   :align: center


.. figure:: images/discours_she.png
   :align: center


.. figure:: images/depart_manifestation.png
   :align: center

   Départ du cortège : à gauche, avec une écharpe aux couleurs de l'Iran,
   la présidente de la LDH Grenoble Zohreh Baharmast.
   © Manuel Pavard - Place Gre'net


.. figure:: images/dans_les_rues.png
   :align: center


.. figure:: images/chants_notre_dame.png
   :align: center


.. figure:: images/place_notre_dame.png
   :align: center


.. figure:: images/place_notre_dame_2.png
   :align: center


.. figure:: images/chants_felix_poulat.png
   :align: center

.. figure:: images/chants_felix_poulat_2.png
   :align: center


.. figure:: images/les_4.png
   :align: center


.. figure:: images/les_3.png
   :align: center

