


==========================
Mai 2023
==========================


2023-05-10 I (Vaclav BALEK) have decided to appoint H.E. Mr. Ali Bahreini, Ambassador and Permanent Representative of the Islamic Republic of Iran,to chair the 2023 Social Forum to be held on 2 and 3 November 2023
===========================================================================================================================================================================================================================

:download:`conseil_des_droits_de_lhomme.png`


Accordingly, in my letter dated 6 April 2023, I invited regional groups
to submit nominations of the candidates for the Chair-Rapporteur of the
2023 Social Forum

--
Following the receipt of a nomination from regional coordinateors, I have
the pleasure to inform you that I have decided to appoint H.E. Mr. Ali Bahreini,
Ambassador and Permanent Representative of the Islamic Republic of Iran,
to chair the 2023 Social Forum to be held on 2 and 3 November 2023, which
will focus on the contribution of science, technology and innovation to
the promotion of human rights, including in the context of post pandemic recovery.


Please accep, Excellencies, the assurances of my highest consideration.

Vaclav BALEK, president of the Human Rights Council.
https://www.ohchr.org/en/hr-bodies/hrc/bio-vaclav-balek
Václav Bálek
Ambassador Václav Bálek has been the Permanent Representative of the Czech
Republic to the United Nations Office and other international organizations
in Geneva since August 2021. On 9 December 2022, Mr. Bálek was elected
president of the Human Rights Council for 2023.
