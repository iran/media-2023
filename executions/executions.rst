
.. _iran_executions_2023:

=========================================
Excutions #StopExecutionsInIran
=========================================


#StopExecutionsInIran
===================================


.. figure:: stop_executions.png
   :align: center
   :width: 500



Vendredi 19 mai 2023 **Pendaisons de Saleh Mirhashemi, Majid Kazemi et Saeed Yaghoubi #SalehMirhashemi, #MajidKazemi, and #SaeedYaghoubi**
==============================================================================================================================================

- :ref:`iran_luttes:pendaisons_2023_05_19`

.. figure:: saleh_saeed_majid.png
   :align: center
   :width: 500

